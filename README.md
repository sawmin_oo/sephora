# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository is for the Sephora Project which is assigned.
* Quick summary
The project is to build a system according to the following requirements.
	- Register SKUs. These are also known as sellable products.
	- Create two Distribution Centers, “Singapore” and “Thailand”.
	- Each product should have inventory levels scoped to Distribution Centers.
	- Create Application Programming Interfaces (APIs) which allows:
	- Addition of inventory (when warehouse receives inventory)
	- Reduction of inventory (when warehouse ships items or manual warehouse
	adjustments)
	- Reservation of inventory (when customer makes a purchase but item is not yet
	shipped from the warehouse)
	- Generate reports (CSV or JSON format) which shows
	- Current Stock On Hand (warehouse
	-----------------------------------------------
	Building a UI is not needed for this exercise. You are free to build additional modules if needed
	to support the workflows.
	Your solution will be accessed based on the following criteria:
	1. Readability. Automated tests are strongly recommended
	2. Performance
	3. Modularity and Separation of Concerns
	4. Database design
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Summary of set up
The project is based on the Django Framework (Django==2.0.6) and Django Restframework (djangorestframework==3.8.2).
Python 3.6.5 is used.
1.Download the repository.
2.create the virtual Environment and activate to use it like belows:
	$ virtualenv my_env
	$ . my_env/bin/activate

* Configuration

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact