from django.contrib import admin
from .models import *
# Register your models here.
class countryadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'Country', 'CountryCode')
    list_filter = ('id',)
    # add search field
    search_fields = ['Country']
admin.site.register(Country,countryadmin)

class regionadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'region_name', 'region_code')
    list_filter = ('id',)
    # add search field
    search_fields = ['region_name']
admin.site.register(Region,regionadmin)

class warehouseadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'name', 'address','phone_no','region_id')
    list_filter = ('id',)
    # add search field
    search_fields = ['name','address']
admin.site.register(Warehouse,warehouseadmin)

class sku_typeadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'type')
    list_filter = ('id',)
    # add search field
    search_fields = ['type']
admin.site.register(Sku_type,sku_typeadmin)

class skuadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'sku_name','sku_description','sku_type_id','sku_price')
    list_filter = ('id',)
    # add search field
    search_fields = ['sku_name','sku_description','sku_price']
admin.site.register(Sku,skuadmin)


class warehouse_skuadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'sku_id','warehouse_id','quantity','inventory_level')
    list_filter = ('id',)
    # add search field
    search_fields = ['quantity']
admin.site.register(Warehouse_sku,warehouse_skuadmin)

class orderadmin(admin.ModelAdmin):
    # define which columns displayed in changelist
    list_display = ('id', 'warehouse_sku_id','order_status','quantity')
    list_filter = ('id',)
    # add search field
    search_fields = ['order_status','quantity']
admin.site.register(Order,orderadmin)