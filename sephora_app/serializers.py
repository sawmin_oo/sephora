from rest_framework import serializers
from sephora_app.models import Country


class CountrySerializer(serializers.Serializer):
    Country = serializers.CharField(required=False, allow_blank=False, max_length=100)
    CountryCode = serializers.CharField(required=False, allow_blank=False, max_length=20)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Country.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.Country = validated_data.get('country', instance.Country)
        instance.CountryCode = validated_data.get('countrycode', instance.CountryCode)
        instance.save()
        return instance
'''
class WarehouseskuSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    sku_id=serializers.IntegerField(required=True)
    warehouse_id = serializers.IntegerField(required=True)
    quantity=serializers.IntegerField(required=True)
    Country = serializers.CharField(required=False, allow_blank=False, max_length=100)
    CountryCode = serializers.CharField(required=False, allow_blank=False, max_length=20)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Country.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.Country = validated_data.get('country', instance.Country)
        instance.CountryCode = validated_data.get('countrycode', instance.CountryCode)
        instance.save()
        return instance
'''
