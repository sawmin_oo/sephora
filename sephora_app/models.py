from django.db import models
class Country(models.Model):
    def __str__(self):
        return self.Country
    id=models.AutoField(primary_key=True)
    Country = models.CharField(max_length=100)
    CountryCode =models.CharField(max_length=5)
    class Meta:
        ordering = ('id',)

class Region(models.Model):
    def __str__(self):
        return self.region_name
    id=models.AutoField(primary_key=True)
    region_name = models.CharField(max_length=100)
    region_code =models.CharField(max_length=5)
    country_id =models.ForeignKey(Country, on_delete=models.CASCADE)
    class Meta:
        ordering = ('id',)

class Warehouse(models.Model):
    def __str__(self):
        return self.name
    id=models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    address =models.CharField(max_length=200)
    phone_no=models.CharField(max_length=20)
    region_id =models.ForeignKey(Region, on_delete=models.CASCADE)
    class Meta:
        ordering = ('id',)


class Sku_type(models.Model):
    def __str__(self):
        return self.type
    id=models.AutoField(primary_key=True)
    type = models.CharField(max_length=100)
    class Meta:
        ordering = ('id',)

class Sku(models.Model):
    def __str__(self):
        return self.sku_name
    id=models.AutoField(primary_key=True)
    sku_name = models.CharField(max_length=100)
    sku_description =models.CharField(max_length=500)
    sku_type_id=models.ForeignKey(Sku_type, on_delete=models.CASCADE)
    sku_price=models.DecimalField(max_digits=5, decimal_places=2)
    class Meta:
        ordering = ('id',)


class Warehouse_sku(models.Model):
    id=models.AutoField(primary_key=True)
    sku_id=models.ForeignKey(Sku, on_delete=models.CASCADE)
    warehouse_id = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    inventory_level = models.IntegerField(default=0)
    quantity=models.IntegerField(default=0)
    class Meta:
        ordering = ('id',)

class Order(models.Model):
    id=models.AutoField(primary_key=True)
    warehouse_sku_id=models.ForeignKey(Sku, on_delete=models.CASCADE)
    order_status=models.CharField(max_length=10)
    quantity=models.IntegerField(default=0)
    class Meta:
        ordering = ('id',)