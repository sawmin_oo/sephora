# Generated by Django 2.0.6 on 2018-06-24 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('Country', models.CharField(blank=True, default='', max_length=100)),
                ('CountryCode', models.TextField()),
            ],
            options={
                'ordering': ('id',),
            },
        ),
    ]
