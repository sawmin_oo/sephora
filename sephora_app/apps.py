from django.apps import AppConfig


class SephoraAppConfig(AppConfig):
    name = 'sephora_app'
